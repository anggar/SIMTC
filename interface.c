#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define WIDTH  90
#define HEIGHT 25

#define REP(i, k) for(int i=0; i<k; i++)
#define FORC(i, j, c) for(int i=j; i<c; i++)
#define REPP(i, j, k) REP(i, k) REP(j, k)

// STRUKTUR DATA DASAR

typedef struct Mahasiswa{
	char    NRP[14];
	char    Nama[50];
	char    Doswal[50];
	double  IPK;
	int     SisaSKS;
	int		JumlahMatkul;
} Mahasiswa;

typedef struct MataKuliah {
	char    Kode[20];
	char    Nama[50];
	char	Dosen[50];
	char    Kelas[10];
	char	Ruang[10];
	int     SKS;
} Matkul;

typedef struct Nilai {
	char 	NRP[14];
	char	Matkul[30];
	char 	Kode[20];
	char	Kelas[2];
	double 	Nilai;
} Nilai;

// BASIC FUNCTION

void setupWindow(){
	system("mode 90,25");
	COORD bufferSize = {WIDTH, HEIGHT};
	SMALL_RECT WinRect   = {0, 0, WIDTH, HEIGHT};
	SMALL_RECT * WinSize = &WinRect;

	SetConsoleTitle("Sistem Informasi Mahasiswa - TC");

	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), 1, WinSize);

	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), bufferSize);
}

void gotoxy(SHORT x, SHORT y){
	COORD cursor = {x, y};

	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
}

void cleanTheBox(){
	FORC(i, 4, HEIGHT-5){
		FORC(j, 16, WIDTH-16){
			gotoxy(j, i); printf("%c", 32);
		}
	}
}

void berhasilDitambahkan(int pos);
void berhasilDihapus(int pos);

// SUGAR CANDY BABY

void mainMenu();
void siMahasiswa(), tambahMahasiswa(), hapusMahasiswa(), lihatMahasiswa(), lihatSemuaMahasiswa(), detailMahasiswa();
void siMatkul(), tambahMatkul(), dropMatkul(), lihatMatkul(), lihatSemuaMatkul(), hapusMatkul(), ambilMatkul();
void siNilai(), inputNilai(), lihatNilai(), lihatSemuaNilai();

void matkulDiambilOleh(char * NRP);
void mahasiswaMengambilMatkul(char * Matkul, char * Kelas);

int main(){
	mainMenu();
}

void berhasilDitambahkan(int pos){
	gotoxy(19, 4+pos-2); printf("DATA BERHASIL DITAMBAHKAN");
	gotoxy(19, 4+pos+2); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+pos+3); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+pos);

	if(getch() == 32) mainMenu();
}

void berhasilDihapus(int pos){
	gotoxy(19, 4+pos-2); printf("DATA BERHASIL DIHAPUS");
	gotoxy(19, 4+pos+2); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+pos+3); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+pos);

	if(getch() == 32) mainMenu();
}

// MAHASISWA

void siMahasiswa(){
	setupWindow();

	system("COLOR 02");
	REP(i, HEIGHT){
		REP(j, WIDTH){
			gotoxy(j, i); printf("%c", 177);
		}
	}

	FORC(i, 3, HEIGHT-4){
		FORC(j, 15, WIDTH-15){
			gotoxy(j, i); printf("%c", 32);

			if(i == 3) 				{ gotoxy(j, i); printf("%c", 196);}
			if(j == 15)				{ gotoxy(j, i); printf("%c", 179);}
			if(i == (HEIGHT-4)-1) 	{ gotoxy(j, i); printf("%c", 196);}
			if(j == (WIDTH-15)-1)	{ gotoxy(j, i); printf("%c", 179);}

			if((i == 3) && (j == 15))						{ gotoxy(j, i); printf("%c", 218);}
			if((i == 3) && (j == (WIDTH-15)-1))				{ gotoxy(j, i); printf("%c", 191);}
			if((i == (HEIGHT-4)-1) && (j == 15))			{ gotoxy(j, i); printf("%c", 192);}
			if((i == (HEIGHT-4)-1) && (j == (WIDTH-15)-1))	{ gotoxy(j, i); printf("%c", 217);}
		}

	}

	gotoxy(17, 3); printf(" MAHASISWA ");

	gotoxy(19, 4+7); printf("1. TAMBAH MAHASISWA");
	gotoxy(19, 4+8); printf("2. LIHAT MAHASISWA");
	gotoxy(19, 4+9); printf("3. LIHAT SEMUA MAHASISWA");
	gotoxy(19, 4+10); printf("4. HAPUS MAHASISWA");
	gotoxy(19, 4+11); printf("9. KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+12); printf("0. KELUAR");

	gotoxy(19, 4+14); printf("> ");

	switch(getch()){
		case '0':
			return;
			break;
		case '1':
			tambahMahasiswa();
			break;
		case '2':
			lihatMahasiswa();
			break;
		case '3':
			lihatSemuaMahasiswa();
			break;
		case '4':
			hapusMahasiswa();
			break;
		case '9':
			mainMenu();
			break;
	}
}

void detailMahasiswa(Mahasiswa MabaNow, int pos){
//	gotoxy(19, 4+pos+0); printf("NRP"); 	 gotoxy(19+9, 4+pos+0); printf(" : %s", MabaNow.NRP);
	gotoxy(19, 4+pos+1); printf("NAMA"); 	 gotoxy(19+9, 4+pos+1); printf(" : %s", MabaNow.Nama);
	gotoxy(19, 4+pos+2); printf("DOSWAL"); 	 gotoxy(19+9, 4+pos+2); printf(" : %s", MabaNow.Doswal);
	gotoxy(19, 4+pos+3); printf("IPK"); 	 gotoxy(19+9, 4+pos+3); printf(" : %.3lf", MabaNow.IPK);
	gotoxy(19, 4+pos+4); printf("SISA SKS"); gotoxy(19+9, 4+pos+4); printf(" : %d", MabaNow.SisaSKS);
}

void tambahMahasiswa(){
	Mahasiswa Maba;

	gotoxy(17, 3); printf(" MAHASISWA > TAMBAH ");

	FILE * writeIt;
	writeIt = fopen("DB_MAHASISWA.dat", "a");

	cleanTheBox();

	gotoxy(19+0, 4+5); printf("NRP");
	gotoxy(19+9, 4+5); printf(": "); scanf("%s", Maba.NRP);

	fflush(stdin);

	gotoxy(19, 4+6); 	printf("NAMA");
	gotoxy(19+9, 4+6); printf(": "); gets(Maba.Nama);

	fflush(stdin);

	gotoxy(19, 4+7); printf("DOSWAL");
	gotoxy(19+9, 4+7); printf(": "); gets(Maba.Doswal);

	Maba.IPK = 0.0;
	Maba.SisaSKS = 144;
	Maba.JumlahMatkul = 0;

	fprintf(writeIt, "\n"); // Line Break
	fprintf(writeIt, "%s\n",  Maba.NRP);
	fprintf(writeIt, "%s\n",  Maba.Nama);
	fprintf(writeIt, "%s\n",  Maba.Doswal);
	fprintf(writeIt, "%lf\n", Maba.IPK);
	fprintf(writeIt, "%d\n",  Maba.SisaSKS);
	fprintf(writeIt, "%d\n",  Maba.JumlahMatkul);

	fclose(writeIt);

	berhasilDitambahkan(12);
}

void lihatMahasiswa(){
	cleanTheBox();

	char * NRP;

	gotoxy(17, 3); printf(" MAHASISWA > LIHAT ");
	gotoxy(19, 4+2); printf("NRP");
	gotoxy(19+9, 4+2); printf(" : "); scanf("%s", NRP);

	Mahasiswa MabaNow;
    int MabaDitemukan = 0;

    FILE * readIt;
    readIt = fopen("DB_MAHASISWA.dat", "r");

    while(fscanf(readIt, "%s", MabaNow.NRP) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(NRP, MabaNow.NRP) == 0){
            MabaDitemukan = 1;

            detailMahasiswa(MabaNow, 3);
		}
    }

    if(!MabaDitemukan){ gotoxy(19, 4+4); printf("MAHASISWA TIDAK DITEMUKAN\n");}

    fclose(readIt);

	if(MabaDitemukan) gotoxy(19, 4+13); printf("TEKAN [M] UNTUK MELIHAT MATKUL YANG DIAMBIL");
	gotoxy(19, 4+14); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+15); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	switch(getch()){
		case 'm':
		case 'M':
			matkulDiambilOleh(NRP);
			break;
		case 32:
			mainMenu();
			break;
	}
}

void mahasiswaMengambilMatkul(char * Matkul, char * Kelas){
	cleanTheBox();

	gotoxy(17, 3); printf(" MATA KULIAH > LIHAT > MAHASISWA \n");

	gotoxy(19, 4+1); printf("MATKUL : %s %s", Matkul, Kelas);

	int counter = 1;

	Nilai NilaiMaba;
	
	FILE * cariMaba;
	FILE * fileOut;
	cariMaba = fopen("DB_NILAI.dat", "r");
	fileOut  = fopen("PESERTA_MATKUL.dat", "w");

	fprintf(fileOut, "Peserta Mata Kuliah %s Kelas %s\n", Matkul, Kelas);

	while(fscanf(cariMaba, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariMaba);
		fscanf(cariMaba, "%s", NilaiMaba.Kode);
		fgetc(cariMaba);
		fgets(NilaiMaba.Matkul, 50, cariMaba);
		fscanf(cariMaba, "%s", NilaiMaba.Kelas);
		fgetc(cariMaba);
		fscanf(cariMaba, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiMaba.Matkul, Matkul) == 0) && (strcmp(NilaiMaba.Kelas, Kelas) == 0)){
			fprintf(fileOut, "%s\n", NilaiMaba.NRP);

			gotoxy(19, 4+2+counter); printf("%s\n", NilaiMaba.NRP);
			counter++;
		}
	}

	fclose(cariMaba);
	fclose(fileOut);

	gotoxy(19, 4+14); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+15); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	if(getch() == 32){
		mainMenu();
	}
}

void lihatSemuaMahasiswa(){
	int counter = 0;
	int counterPos = 0;
	fpos_t pos[100] = {0};

	cleanTheBox();
	gotoxy(17, 3); printf(" MAHASISWA > SEMUA ");

	Mahasiswa MabaNow;

	FILE * readIt;
	readIt = fopen("DB_MAHASISWA.dat", "r");

	while(1){
		if(counter == 0){
			fgetpos(readIt, &pos[counterPos]);
			counterPos++;
		}
		if(fscanf(readIt, "%s", MabaNow.NRP) == EOF) break;
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		detailMahasiswa(MabaNow, counter*5);
		gotoxy(WIDTH-24, 4+(counter*5)+1); printf("(%s)", MabaNow.NRP);
		counter++;

		gotoxy(16, 4+16);
		if(counterPos != 1) printf(" < PREV ");
		else printf("%c%c%c%c%c%c%c%c", 196, 196, 196, 196 , 196, 196, 196, 196);
		gotoxy(WIDTH-24, 4+16); printf(" NEXT > ");

		if(counter == 3){
			gotoxy(WIDTH-18, 4+16);

			backAllMahasiswa:
			switch(getch()){
				case 224:
					switch(getch()){
						case 77: 
							cleanTheBox();
							counter = 0;
							break;
						case 75:
							prevAllMahasiswa:
							cleanTheBox();
							counter = 0;
							counterPos -= 2;
							if(counterPos < 0) counterPos = 0;
							fsetpos(readIt, &pos[counterPos]);
							break;
					}
					break;
				case 32:
					mainMenu();
					break;
				default:
					goto backAllMahasiswa;
			}
		}
	}

	gotoxy(19, 4+(counter*5)+1); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+(counter*5)+2); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+(counter*5)+4);
	switch(getch()){
		case 32: mainMenu(); break;
		case 224:
			if(getch() == 75) goto prevAllMahasiswa;
	}

	fclose(readIt);
}

void hapusMahasiswa(){
	char * NRP;
	cleanTheBox();

	gotoxy(17, 3); printf(" MAHASISWA > HAPUS ");

	gotoxy(19+0, 4+2); printf("NRP");
	gotoxy(19+9, 4+2); printf(" :  "); scanf("%s", NRP);

	Mahasiswa MabaNow;
    int MabaDitemukan = 0;
    int WantToDelete = 1;

    FILE * readIt;
    FILE * tempCopy;
    readIt   = fopen("DB_MAHASISWA.dat", "r");
    tempCopy = fopen("DB_MAHASISWA.COPY.dat", "w");

    while(fscanf(readIt, "%s", MabaNow.NRP) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(NRP, MabaNow.NRP) == 0){ 
			MabaDitemukan = 1;

			detailMahasiswa(MabaNow, 3);
			gotoxy(19, 4+12); printf("ANDA YAKIN INGIN MENGHAPUS DATA? [Y/N] ");
	    	
	    	switch(getch()){
	    		case 'n':
	    		case 'N': WantToDelete=0; goto wontdelete; break;
	    	}
		}

		if(strcmp(NRP, MabaNow.NRP) != 0){
			wontdelete:
            fprintf(tempCopy, "\n");
            fprintf(tempCopy, "%s\n", MabaNow.NRP);
			fprintf(tempCopy, "%s", MabaNow.Nama);
			fprintf(tempCopy, "%s", MabaNow.Doswal);
			fprintf(tempCopy, "%lf\n", MabaNow.IPK);
			fprintf(tempCopy, "%d\n", MabaNow.SisaSKS);
			fprintf(tempCopy, "%d\n", MabaNow.JumlahMatkul);
		}

		gotoxy(19, 4+12); printf("                                      ");
    }

    if(!MabaDitemukan){
    	WantToDelete = 0;
    	gotoxy(19, 4+4); printf("DATA MAHASISWA TIDAK DITEMUKAN!\n");
    }

    fclose(readIt);
    fclose(tempCopy);

    readIt = fopen("DB_MAHASISWA.dat", "w");
    tempCopy = fopen("DB_MAHASISWA.COPY.dat", "r");

	while(fscanf(tempCopy, "%s", MabaNow.NRP) != EOF){
        fgetc(tempCopy);                              // Newline handling
		fgets(MabaNow.Nama, 50, tempCopy);
		fgets(MabaNow.Doswal, 50, tempCopy);
		fscanf(tempCopy, "%lf", &MabaNow.IPK);
		fscanf(tempCopy, "%d", &MabaNow.SisaSKS);
		fscanf(tempCopy, "%d", &MabaNow.JumlahMatkul);

        fprintf(readIt, "\n");
        fprintf(readIt, "%s\n", MabaNow.NRP);
		fprintf(readIt, "%s", MabaNow.Nama);
		fprintf(readIt, "%s", MabaNow.Doswal);
		fprintf(readIt, "%lf\n", MabaNow.IPK);
		fprintf(readIt, "%d\n", MabaNow.SisaSKS);
		fprintf(readIt, "%d\n", MabaNow.JumlahMatkul);
    }

    fclose(readIt);
    fclose(tempCopy);

	if(WantToDelete) berhasilDihapus(12);
	else {
		gotoxy(19, 4+10); printf("                                         ");
		gotoxy(19, 4+14); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
		gotoxy(19, 4+15); printf("TEKAN LAINNYA UNTUK KELUAR");

		gotoxy(19, 4+12);

		if(getch() == 32) mainMenu();
	}
}

// MATKUL

void siMatkul(){
	setupWindow();

	system("COLOR 03");
	REP(i, HEIGHT){
		REP(j, WIDTH){
			gotoxy(j, i); printf("%c", 177);
		}
	}

	FORC(i, 3, HEIGHT-4){
		FORC(j, 15, WIDTH-15){
			gotoxy(j, i); printf("%c", 32);

			if(i == 3) 				{ gotoxy(j, i); printf("%c", 196);}
			if(j == 15)				{ gotoxy(j, i); printf("%c", 179);}
			if(i == (HEIGHT-4)-1) 	{ gotoxy(j, i); printf("%c", 196);}
			if(j == (WIDTH-15)-1)	{ gotoxy(j, i); printf("%c", 179);}

			if((i == 3) && (j == 15))						{ gotoxy(j, i); printf("%c", 218);}
			if((i == 3) && (j == (WIDTH-15)-1))				{ gotoxy(j, i); printf("%c", 191);}
			if((i == (HEIGHT-4)-1) && (j == 15))			{ gotoxy(j, i); printf("%c", 192);}
			if((i == (HEIGHT-4)-1) && (j == (WIDTH-15)-1))	{ gotoxy(j, i); printf("%c", 217);}
		}

	}

	gotoxy(17, 3); printf(" MATA KULIAH ");

	gotoxy(19, 4+5); printf("1. TAMBAH MATA KULIAH");
	gotoxy(19, 4+6); printf("2. AMBIL MATA KULIAH");
	gotoxy(19, 4+7); printf("3. DROP MATA KULIAH");
	gotoxy(19, 4+8); printf("4. LIHAT MATA KULIAH");
	gotoxy(19, 4+9); printf("5. LIHAT SEMUA MATA KULIAH");
	gotoxy(19, 4+10); printf("6. HAPUS MATA KULIAH");
	gotoxy(19, 4+11); printf("9. KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+12); printf("0. KELUAR");

	gotoxy(19, 4+14); printf("> ");

	switch(getch()){
		case '0':
			return;
			break;
		case '9':
			mainMenu();
			break;
		case '1':
			tambahMatkul();
			break;
		case '2':
			ambilMatkul();
			break;
		case '3':
			dropMatkul();
			break;
		case '4':
			lihatMatkul();
			break;
		case '5':
			lihatSemuaMatkul();
			break;
		case '6':
			hapusMatkul();
			break;
	}
}

void matkulDiambilOleh(char * NRP){
	cleanTheBox();

	gotoxy(17, 3); printf(" MAHASISWA > LIHAT > MATKUL \n");

	gotoxy(19, 4+1); printf("NRP : %s", NRP);

	int counter = 1;

	Nilai NilaiMaba;
	
	FILE * cariMatkul;
	FILE * fileOut;
	cariMatkul = fopen("DB_NILAI.dat", "r");
	fileOut    = fopen("MATKUL_DIAMBIL.dat", "w");

	fprintf(fileOut, "Mata Kuliah yang di ambil oleh %s\n", NRP);

	while(fscanf(cariMatkul, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariMatkul);
		fscanf(cariMatkul, "%s", NilaiMaba.Kode);
		fgetc(cariMatkul);
		fgets(NilaiMaba.Matkul, 50, cariMatkul);
		fscanf(cariMatkul, "%s", NilaiMaba.Kelas);
		fgetc(cariMatkul);
		fscanf(cariMatkul, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if(strcmp(NilaiMaba.NRP, NRP) == 0){
			fprintf(fileOut, "%s %s\n", NilaiMaba.Matkul, NilaiMaba.Kelas);

			gotoxy(19, 4+2+counter); printf("%s %s", NilaiMaba.Matkul, NilaiMaba.Kelas);
		}
	}

	fclose(cariMatkul);
	fclose(fileOut);

	gotoxy(19, 4+14); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+15); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	if(getch() == 32){
		mainMenu();
	}
}

void detailMatkul(Matkul MKNow, int pos){
	gotoxy(19, 4+pos+1); printf("KODE"); 	 gotoxy(19+9, 4+pos+1); printf(" : %s", MKNow.Kode);
	gotoxy(19, 4+pos+2); printf("MATKUL"); 	 gotoxy(19+9, 4+pos+2); printf(" : %s", MKNow.Nama);
	gotoxy(19, 4+pos+3); printf("DOSEN"); 	 gotoxy(19+9, 4+pos+3); printf(" : %s", MKNow.Dosen);
	gotoxy(19, 4+pos+4); printf("KELAS"); 	 gotoxy(19+9, 4+pos+4); printf(" : %s", MKNow.Kelas);
	gotoxy(19, 4+pos+5); printf("RUANG"); 	 gotoxy(19+9, 4+pos+5); printf(" : %s", MKNow.Ruang);
	gotoxy(19, 4+pos+6); printf("SKS"); 	 gotoxy(19+9, 4+pos+6); printf(" : %d", MKNow.SKS);
}

void tambahMatkul(){
	cleanTheBox();

	gotoxy(17, 3); printf(" MATA KULIAH > TAMBAH");

	Matkul MK;

	FILE * writeIt;
	writeIt = fopen("DB_MATKUL.dat", "a");

	gotoxy(19, 4+4); printf("KODE");
	gotoxy(19+9, 4+4); printf(": "); scanf("%s", MK.Kode);

	fflush(stdin);

	gotoxy(19, 4+5); printf("NAMA MK");
	gotoxy(19+9, 4+5); printf(": "); gets(MK.Nama);

	fflush(stdin);

	gotoxy(19, 4+6); printf("DOSEN");
	gotoxy(19+9, 4+6); printf(": "); gets(MK.Dosen);

	fflush(stdin);

	gotoxy(19, 4+7); printf("KELAS");
	gotoxy(19+9, 4+7); printf(": "); scanf("%s", MK.Kelas);

	fflush(stdin);

	gotoxy(19, 4+8); printf("RUANG");
	gotoxy(19+9, 4+8); printf(": "); scanf("%s", MK.Ruang);

	fflush(stdin);

	gotoxy(19, 4+9); printf("SKS");
	gotoxy(19+9, 4+9); printf(": "); scanf("%d", &MK.SKS);

	fprintf(writeIt, "\n");
	fprintf(writeIt, "%s\n", MK.Kode);
	fprintf(writeIt, "%s\n", MK.Nama);
	fprintf(writeIt, "%s\n", MK.Dosen);
	fprintf(writeIt, "%s\n", MK.Kelas);
	fprintf(writeIt, "%s\n", MK.Ruang);
	fprintf(writeIt, "%d\n", MK.SKS);

	fclose(writeIt);

	berhasilDitambahkan(12);
}

void lihatMatkul(){
	cleanTheBox();

	char Kelas[50];
	char MK[50];

	fflush(stdin);
	gotoxy(17, 3); printf(" MATA KULIAH > LIHAT ");
	gotoxy(19, 4+2); printf("MATKUL");
	gotoxy(19+9, 4+2); printf(" : "); gets(MK);
	gotoxy(19, 4+3); printf("KELAS");
	gotoxy(19+9, 4+3); printf(" : "); scanf("%s", Kelas);

	Matkul MatkulNow;
    int MatkulDitemukan = 0;

    FILE * readIt;
    readIt = fopen("DB_MATKUL.dat", "r");

    while(1){
    	if(fscanf(readIt, "%s", MatkulNow.Kode) == EOF) break;
        fgetc(readIt);                              // Newline handling
		fgets(MatkulNow.Nama, 50, readIt);
		fgets(MatkulNow.Dosen, 50, readIt);
		fscanf(readIt, "%s", MatkulNow.Kelas);
		fgetc(readIt);
		fscanf(readIt, "%s", MatkulNow.Ruang);
		fgetc(readIt);
		fscanf(readIt, "%d", &MatkulNow.SKS);

		MatkulNow.Nama[strlen(MatkulNow.Nama)-1] = '\0';

		if((strcmp(MK, MatkulNow.Nama) == 0) && (strcmp(Kelas, MatkulNow.Kelas) == 0)){
            MatkulDitemukan = 1;

            gotoxy(19, 4+4+1); printf("KODE"); 	 	 gotoxy(19+9, 4+4+1); printf(" : %s", MatkulNow.Kode);
			gotoxy(19, 4+4+2); printf("DOSEN"); 	 gotoxy(19+9, 4+4+2); printf(" : %s", MatkulNow.Dosen);
			gotoxy(19, 4+4+3); printf("RUANG"); 	 gotoxy(19+9, 4+4+3); printf(" : %s", MatkulNow.Ruang);
			gotoxy(19, 4+4+4); printf("SKS"); 		 gotoxy(19+9, 4+4+4); printf(" : %d", MatkulNow.SKS);
		}
    }

    if(!MatkulDitemukan){ gotoxy(19, 4+7); printf("MATA KULIAH TIDAK DITEMUKAN!\n");}

	fclose(readIt);

	if(MatkulDitemukan){ gotoxy(19, 4+13); printf("TEKAN [M] UNTUK MELIHAT MAHASISWA YANG MENGAMBIL");}
	gotoxy(19, 4+14); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+15); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	switch(getch()){
		case 'm':
		case 'M':
			mahasiswaMengambilMatkul(MK, Kelas);
			break;
		case 32:
			mainMenu();
			break;
	}
}

void lihatSemuaMatkul(){
	int counter = 0;
	int counterPos = 0;
	fpos_t pos[100] = {0};

	cleanTheBox();
	gotoxy(17, 3); printf(" MATA KULIAH > SEMUA ");

	Matkul MK;

	FILE * readIt;
	readIt = fopen("DB_MATKUL.dat", "r");

	while(1){
		if(counter == 0){
			fgetpos(readIt, &pos[counterPos]);
			counterPos++;
		}
		if(fscanf(readIt, "%s", MK.Kode) == EOF) break;
        fgetc(readIt);                              // Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fgetc(readIt);
		fscanf(readIt, "%s", MK.Ruang);
		fgetc(readIt);
		fscanf(readIt, "%d", &MK.SKS);

		detailMatkul(MK, counter*7);
		counter++;

		gotoxy(16, 4+16);
		if(counterPos != 1) printf(" < PREV ");
		else printf("%c%c%c%c%c%c%c%c", 196, 196, 196, 196 , 196, 196, 196, 196);
		gotoxy(WIDTH-24, 4+16); printf(" NEXT > ");

		if(counter == 2){
			gotoxy(WIDTH-18, 4+16);

			backAllMatkul:
			switch(getch()){
				case 224:
					switch(getch()){
						case 77: 
							cleanTheBox();
							counter = 0;
							break;
						case 75:
							prevAllMatkul:
							cleanTheBox();
							counter = 0;
							counterPos -= 2;
							if(counterPos < 0) counterPos = 0;
							fsetpos(readIt, &pos[counterPos]);
							break;
					}
					break;
				case 32:
					mainMenu();
					break;
				default:
					goto backAllMatkul;
			}
		}
	}

	gotoxy(19, 4+(counter*7)+1); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+(counter*7)+2); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+(counter*7)+4);
	switch(getch()){
		case 32: mainMenu(); break;
		case 224:
			if(getch() == 75) goto prevAllMatkul;
	}

	fclose(readIt);
}

void hapusMatkul(){
	char Namkul[50];
	char Kelen[10];

	cleanTheBox();

	gotoxy(17, 3); printf(" MATA KULIAH > HAPUS ");

	fflush(stdin);

	gotoxy(19+0, 4+2); printf("MATKUL");
	gotoxy(19+9, 4+2); printf(" : "); gets(Namkul);

	gotoxy(19+0, 4+3); printf("KELAS");
	gotoxy(19+9, 4+3); printf(" : "); scanf("%s", Kelen);

	Matkul MK;
	int MatkulDitemukan = 0;
	int WantToDelete = 1;

	FILE * readIt;
	FILE * tempCopy;

	readIt   = fopen("DB_MATKUL.dat", "r");
	tempCopy = fopen("DB_MATKUL.COPY.dat", "w");

	while(fscanf(readIt, "%s", MK.Kode) != EOF){
		fgetc(readIt);								// Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fscanf(readIt, "%s", MK.Ruang);
		fscanf(readIt, "%d", &MK.SKS);

		MK.Nama[strlen(MK.Nama) - 1] = '\0';
		MK.Dosen[strlen(MK.Dosen) - 1] = '\0';

		if((strcmp(Namkul, MK.Nama) == 0) && (strcmp(Kelen, MK.Kelas) == 0)){
			MatkulDitemukan = 1;

			gotoxy(19, 4+4); printf("KODE      : %s", MK.Kode);
			gotoxy(19, 4+5); printf("DOSEN     : %s", MK.Dosen);
			gotoxy(19, 4+6); printf("RUANG     : %s", MK.Ruang);
			gotoxy(19, 4+7); printf("KELAS     : %d", MK.SKS);

			gotoxy(19, 4+9); printf("ANDA YAKIN INGIN MENGHAPUS DATA DI ATAS? [Y/N] ");
			switch(getch()){
				case 'n':
				case 'N': goto wontDeleteClass; break;
			}
		}

		if((strcmp(Namkul, MK.Nama) != 0) || (strcmp(Kelen, MK.Kelas) != 0)){
			wontDeleteClass:
			fprintf(tempCopy, "\n");
			fprintf(tempCopy, "%s\n", MK.Kode);
			fprintf(tempCopy, "%s\n", MK.Nama);
			fprintf(tempCopy, "%s\n", MK.Dosen);
			fprintf(tempCopy, "%s\n", MK.Kelas);
			fprintf(tempCopy, "%s\n", MK.Ruang);
			fprintf(tempCopy, "%d\n", MK.SKS);
		}

	}

	fclose(readIt);
	fclose(tempCopy);

	readIt = fopen("DB_MATKUL.COPY.dat", "r");
	tempCopy   = fopen("DB_MATKUL.dat", "w");

	while(fscanf(readIt, "%s", MK.Kode) != EOF){
		fgetc(readIt);								// Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fscanf(readIt, "%s", MK.Ruang);
		fscanf(readIt, "%d", &MK.SKS);

		MK.Nama[strlen(MK.Nama) - 1] = '\0';
		MK.Dosen[strlen(MK.Dosen) - 1] = '\0';

		fprintf(tempCopy, "\n");
		fprintf(tempCopy, "%s\n", MK.Kode);
		fprintf(tempCopy, "%s\n", MK.Nama);
		fprintf(tempCopy, "%s\n", MK.Dosen);
		fprintf(tempCopy, "%s\n", MK.Kelas);
		fprintf(tempCopy, "%s\n", MK.Ruang);
		fprintf(tempCopy, "%d\n", MK.SKS);
	}

	fclose(readIt);
	fclose(tempCopy);

	if(MatkulDitemukan) berhasilDihapus(12);
	else {
		gotoxy(19, 4+10); printf("DATA MAHASISWA TIDAK DITEMUKAN!");

		gotoxy(19, 4+13); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
		gotoxy(19, 4+14); printf("TEKAN LAINNYA UNTUK KELUAR");

		gotoxy(19, 4+12);
		if(getch() == 32) mainMenu();
	}
}

void ambilMatkul(){
	cleanTheBox();

	gotoxy(17, 3); printf(" MATA KULIAH > AMBIL ");

	Nilai FRS;
	Matkul MK;

	gotoxy(19+0, 4+5); printf("NRP");
	gotoxy(19+9, 4+5); printf(" : "); scanf("%s", FRS.NRP);

	fflush(stdin);

	gotoxy(19+0, 4+6); printf("NAMA MK");
	gotoxy(19+9, 4+6); printf(" : "); gets(FRS.Matkul);

	fflush(stdin);

	gotoxy(19+0, 4+7); printf("KELAS");
	gotoxy(19+9, 4+7); printf(" : "); gets(FRS.Kelas);

	fflush(stdin);

	FILE * searchIt;
	FILE * modifySKS;
	FILE * modifiedSKS;

	Mahasiswa MabaNow;
	int SKSTerambil = 0;

	searchIt  = fopen("DB_MATKUL.dat", "r");
	modifySKS = fopen("DB_MAHASISWA.dat", "r");
	modifiedSKS = fopen("DB_MAHASISWA.COPY.dat", "w");

	while(fscanf(searchIt, "%s", MK.Kode) != EOF){
        fgetc(searchIt);                              // Newline handling
		fgets(MK.Nama, 50, searchIt);
		fgets(MK.Dosen, 50, searchIt);
		fscanf(searchIt, "%s", MK.Kelas);
		fgetc(searchIt);
		fscanf(searchIt, "%s", MK.Ruang);
		fgetc(searchIt);
		fscanf(searchIt, "%d", &MK.SKS);

		MK.Nama[strlen(MK.Nama) - 1] = '\0';		// Penghapusan \n

		if(strcmp(FRS.Matkul, MK.Nama) == 0){
			strcpy(FRS.Kode, MK.Kode);
			SKSTerambil = MK.SKS;
		}
    }

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(MabaNow.NRP, FRS.NRP) == 0){
			MabaNow.SisaSKS -= SKSTerambil;
		}

		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(searchIt);
    fclose(modifySKS);
    fclose(modifiedSKS);

    modifySKS   = fopen("DB_MAHASISWA.COPY.dat", "r");
    modifiedSKS = fopen("DB_MAHASISWA.dat", "w");

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(modifySKS);
    fclose(modifiedSKS);

    FILE * writeIt;
    writeIt = fopen("DB_NILAI.dat", "a");

    fprintf(writeIt, "\n");
    fprintf(writeIt, "%s\n", 	FRS.NRP);
    fprintf(writeIt, "%s\n", 	FRS.Kode);
    fprintf(writeIt, "%s\n", 	FRS.Matkul);
    fprintf(writeIt, "%s\n", 	FRS.Kelas);
    fprintf(writeIt, "%lf\n",   0.0);

    fclose(writeIt);

    berhasilDitambahkan(12);
}

void dropMatkul(){
	cleanTheBox();

	gotoxy(17, 3); printf(" MATA KULIAH > DROP ");

	Nilai FRS;
	Matkul MK;

	int wantToDrop = 1;

	gotoxy(19+0, 4+5); printf("NRP");
	gotoxy(19+9, 4+5); printf(" : "); scanf("%s", FRS.NRP);

	fflush(stdin);

	gotoxy(19+0, 4+6); printf("NAMA MK");
	gotoxy(19+9, 4+6); printf(" : "); gets(FRS.Matkul);

	fflush(stdin);

	gotoxy(19+0, 4+7); printf("KELAS");
	gotoxy(19+9, 4+7); printf(" : "); gets(FRS.Kelas);

	fflush(stdin);

	FILE * searchIt;
	FILE * modifySKS;
	FILE * modifiedSKS;

	Mahasiswa MabaNow;
	int SKSMK = 0;

	searchIt  = fopen("DB_MATKUL.dat", "r");
	modifySKS = fopen("DB_MAHASISWA.dat", "r");
	modifiedSKS = fopen("DB_MAHASISWA.COPY.dat", "w");

	while(fscanf(searchIt, "%s", MK.Kode) != EOF){
        fgetc(searchIt);                              // Newline handling
		fgets(MK.Nama, 50, searchIt);
		fgets(MK.Dosen, 50, searchIt);
		fscanf(searchIt, "%s", MK.Kelas);
		fgetc(searchIt);
		fscanf(searchIt, "%s", MK.Ruang);
		fgetc(searchIt);
		fscanf(searchIt, "%d", &MK.SKS);

		MK.Nama[strlen(MK.Nama) - 1] = '\0';		// Penghapusan \n

		if(strcmp(FRS.Matkul, MK.Nama) == 0){
			strcpy(FRS.Kode, MK.Kode);
			SKSMK = MK.SKS;
		}
    }

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(MabaNow.NRP, FRS.NRP) == 0){
			gotoxy(19, 4+9); printf("ANDA YAKIN INGIN DROP MATA KULIAH TERSEBUT? [Y/N] ");
			switch(getch()){
				case 'n':
				case 'N': wantToDrop=0; goto sksTetap; break;
			}

			MabaNow.SisaSKS += SKSMK;
		}

		sksTetap:
		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(searchIt);
    fclose(modifySKS);
    fclose(modifiedSKS);

    modifySKS   = fopen("DB_MAHASISWA.COPY.dat", "r");
    modifiedSKS = fopen("DB_MAHASISWA.dat", "w");

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(modifySKS);
    fclose(modifiedSKS);

    Nilai NilaiMaba;

    FILE * readIt;
    FILE * tempCopy;
    readIt  = fopen("DB_NILAI.dat", "r");
    tempCopy = fopen("DB_NILAI.COPY.dat", "w");

    while(fscanf(readIt, "%s", NilaiMaba.NRP) != EOF){
    	fgetc(readIt);									// Newline handling
		fscanf(readIt, "%s", NilaiMaba.Kode);
    	fgetc(readIt);
		fgets(NilaiMaba.Matkul, 50, readIt);
		fscanf(readIt, "%s", NilaiMaba.Kelas);
    	fgetc(readIt);
		fscanf(readIt, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(FRS.Matkul, NilaiMaba.Matkul) == 0) && (strcmp(FRS.NRP, NilaiMaba.NRP) == 0) && (strcmp(FRS.Kelas, NilaiMaba.Kelas) == 0)){
			if(NilaiMaba.Nilai != 0){
				gotoxy(19, 4+9); printf("NIlAI TELAH DIMASUKKAN");
				gotoxy(19, 4+10); printf("ANDA TIDAK DAPAT DROP MATA KULIAH INI!\n");
			}
			if(wantToDrop) continue;
		}

	    fprintf(tempCopy, "\n");
	    fprintf(tempCopy, "%s\n", 	NilaiMaba.NRP);
	    fprintf(tempCopy, "%s\n", 	NilaiMaba.Kode);
	    fprintf(tempCopy, "%s\n", 	NilaiMaba.Matkul);
	    fprintf(tempCopy, "%s\n", 	NilaiMaba.Kelas);
	    fprintf(tempCopy, "%lf\n",    NilaiMaba.Nilai);
    }

    fclose(readIt);
    fclose(tempCopy);

    gotoxy(19, 4+12+2); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+12+3); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	if(getch() == 32) mainMenu();
}

// NILAI

void siNilai(){
	setupWindow();

	system("COLOR 06");
	REP(i, HEIGHT){
		REP(j, WIDTH){
			gotoxy(j, i); printf("%c", 177);
		}
	}

	FORC(i, 3, HEIGHT-4){
		FORC(j, 15, WIDTH-15){
			gotoxy(j, i); printf("%c", 32);

			if(i == 3) 				{ gotoxy(j, i); printf("%c", 196);}
			if(j == 15)				{ gotoxy(j, i); printf("%c", 179);}
			if(i == (HEIGHT-4)-1) 	{ gotoxy(j, i); printf("%c", 196);}
			if(j == (WIDTH-15)-1)	{ gotoxy(j, i); printf("%c", 179);}

			if((i == 3) && (j == 15))						{ gotoxy(j, i); printf("%c", 218);}
			if((i == 3) && (j == (WIDTH-15)-1))				{ gotoxy(j, i); printf("%c", 191);}
			if((i == (HEIGHT-4)-1) && (j == 15))			{ gotoxy(j, i); printf("%c", 192);}
			if((i == (HEIGHT-4)-1) && (j == (WIDTH-15)-1))	{ gotoxy(j, i); printf("%c", 217);}
		}

	}

	gotoxy(17, 3); printf(" NILAI ");

	gotoxy(19, 4+8); printf("1. MASUKKAN NILAI");
	gotoxy(19, 4+9); printf("2. LIHAT NILAI");
	gotoxy(19, 4+10); printf("3. LIHAT SEMUA NILAI");
	gotoxy(19, 4+11); printf("9. KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+12); printf("0. KELUAR");

	gotoxy(19, 4+14); printf("> ");

	switch(getch()){
		case '0':
			return;
			break;
		case '1':
			inputNilai();
			break;
		case '2':
			lihatNilai();
			break;
		case '3':
			lihatSemuaNilai();
			break;
		case '9':
			mainMenu();
			break;
	}
}

void inputNilai(){
	cleanTheBox();

	gotoxy(17, 3); printf(" NILAI > TAMBAH ");

	Nilai NilaiBaru;

	gotoxy(19, 4+2); printf("NRP         : ");
	scanf("%s", NilaiBaru.NRP);

	fflush(stdin);

	gotoxy(19, 4+3); printf("MATKUL      : ");
	gets(NilaiBaru.Matkul);

	fflush(stdin);

	do {
		gotoxy(19, 4+4); printf("NILAI (0-4) : ");
		scanf("%lf", &NilaiBaru.Nilai);
	} while((NilaiBaru.Nilai < 0) || (NilaiBaru.Nilai > 4));

	fflush(stdin);

	Nilai NilaiMaba;

	FILE * changeIt;
	FILE * changed;

	changeIt = fopen("DB_NILAI.dat", "r+");
	changed  = fopen("DB_NILAI.COPY.dat", "w");

	while(fscanf(changeIt, "%s", NilaiMaba.NRP) != EOF){
		fgetc(changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kode);
		fgetc(changeIt);
		fgets(NilaiMaba.Matkul, 50, changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kelas);
		fgetc(changeIt);
		fscanf(changeIt, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiBaru.NRP, NilaiMaba.NRP) == 0) && (strcmp(NilaiBaru.Matkul, NilaiMaba.Matkul) == 0)){
			NilaiMaba.Nilai = NilaiBaru.Nilai;
		}

	    fprintf(changed, "\n");
	    fprintf(changed, "%s\n", 	NilaiMaba.NRP);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kode);
	    fprintf(changed, "%s\n", 	NilaiMaba.Matkul);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kelas);
	    fprintf(changed, "%lf\n",   NilaiMaba.Nilai);
	}

	fclose(changeIt);
	fclose(changed);

	changed = fopen("DB_NILAI.dat", "w");
	changeIt  = fopen("DB_NILAI.COPY.dat", "r");

	while(fscanf(changeIt, "%s", NilaiMaba.NRP) != EOF){
		fgetc(changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kode);
		fgetc(changeIt);
		fgets(NilaiMaba.Matkul, 50, changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kelas);
		fgetc(changeIt);
		fscanf(changeIt, "%lf", &NilaiMaba.Nilai);

	    fprintf(changed, "\n");
	    fprintf(changed, "%s\n", 	NilaiMaba.NRP);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kode);
	    fprintf(changed, "%s",	 	NilaiMaba.Matkul);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kelas);
	    fprintf(changed, "%lf\n",   NilaiMaba.Nilai);
	}

	fclose(changeIt);
	fclose(changed);

	Mahasiswa MabaNow;

	changeIt = fopen("DB_MAHASISWA.dat", "r");
	changed  = fopen("DB_MAHASISWA.COPY.dat", "w");

	while(fscanf(changeIt, "%s", MabaNow.NRP) != EOF){
        fgetc(changeIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, changeIt);
		fgets(MabaNow.Doswal, 50, changeIt);
		fscanf(changeIt, "%lf", &MabaNow.IPK);
		fscanf(changeIt, "%d", &MabaNow.SisaSKS);
		fscanf(changeIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(MabaNow.NRP, NilaiBaru.NRP) == 0){
			MabaNow.JumlahMatkul += 1;
			MabaNow.IPK = (MabaNow.IPK * (MabaNow.JumlahMatkul-1)) + NilaiBaru.Nilai;
			MabaNow.IPK = MabaNow.IPK/MabaNow.JumlahMatkul;
		}

		fprintf(changed, "\n");
		fprintf(changed, "%s\n", MabaNow.NRP);
		fprintf(changed, "%s", MabaNow.Nama);
		fprintf(changed, "%s", MabaNow.Doswal);
		fprintf(changed, "%lf\n", MabaNow.IPK);
		fprintf(changed, "%d\n", MabaNow.SisaSKS);
		fprintf(changed, "%d\n", MabaNow.JumlahMatkul);
	}	

	fclose(changed);
	fclose(changeIt);

	changed = fopen("DB_MAHASISWA.dat", "w");
	changeIt  = fopen("DB_MAHASISWA.COPY.dat", "r");

	while(fscanf(changeIt, "%s", MabaNow.NRP) != EOF){
        fgetc(changeIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, changeIt);
		fgets(MabaNow.Doswal, 50, changeIt);
		fscanf(changeIt, "%lf", &MabaNow.IPK);
		fscanf(changeIt, "%d", &MabaNow.SisaSKS);
		fscanf(changeIt, "%d", &MabaNow.JumlahMatkul);

		fprintf(changed, "\n");
		fprintf(changed, "%s\n", MabaNow.NRP);
		fprintf(changed, "%s", MabaNow.Nama);
		fprintf(changed, "%s", MabaNow.Doswal);
		fprintf(changed, "%lf\n", MabaNow.IPK);
		fprintf(changed, "%d\n", MabaNow.SisaSKS);
		fprintf(changed, "%d\n", MabaNow.JumlahMatkul);
	}	

	fclose(changed);
	fclose(changeIt);

	berhasilDitambahkan(12);
}

void lihatNilai(){
	char NRP[20];
	char Kode[10];

	cleanTheBox();
	gotoxy(17, 3); printf(" NILAI > LIHAT ");

	gotoxy(19, 4+2); printf("NRP         : "); scanf("%s", NRP);
	gotoxy(19, 4+3); printf("KODE MATKUL : "); scanf("%s", Kode);

	Nilai NilaiMaba;
	int NilaiDitemukan = 0;
	
	FILE * cariNilai;
	FILE * fileOut;
	cariNilai = fopen("DB_NILAI.dat", "r");
//	char filename[] = strcat(strcat(strcat(strcat("NILAI_", Kode), "_"), NRP), ".dat");
	fileOut   = fopen("NILAI_DARI.dat", "w");

	fprintf(fileOut, "Nilai dari %s pada mata kuliah %s\n", NRP, Kode);

	while(fscanf(cariNilai, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariNilai);
		fscanf(cariNilai, "%s", NilaiMaba.Kode);
		fgetc(cariNilai);
		fgets(NilaiMaba.Matkul, 50, cariNilai);
		fscanf(cariNilai, "%s", NilaiMaba.Kelas);
		fgetc(cariNilai);
		fscanf(cariNilai, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiMaba.NRP, NRP) == 0) && (strcmp(NilaiMaba.Kode, Kode) == 0)){
			NilaiDitemukan = 1;

			fprintf(fileOut, "KELAS       : %s %s\n", NilaiMaba.Matkul, NilaiMaba.Kelas);
			fprintf(fileOut, "NILAI       : %lf", NilaiMaba.Nilai);

			gotoxy(19, 4+5); printf("KELAS       : %s %s", NilaiMaba.Matkul, NilaiMaba.Kelas);
			gotoxy(19, 4+6); printf("NILAI       : %lf", NilaiMaba.Nilai);
		}
	}

	if(!NilaiDitemukan){ gotoxy(19, 4+6); printf("NILAI TIDAK DITEMUKAN!");}

	fclose(cariNilai);
	fclose(fileOut);

	gotoxy(19, 4+12+2); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+12+3); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+12);

	if(getch() == 32) mainMenu();
}

void lihatSemuaNilai(){
	int counter = 0;
	int counterPos = 0;
	fpos_t pos[100] = {0};

	cleanTheBox();
	gotoxy(17, 3); printf(" NILAI > SEMUA ");

	Nilai NilaiNow;

	FILE * readIt;
	readIt = fopen("DB_NILAI.dat", "r");

	while(1){
		if(counter == 0){
			fgetpos(readIt, &pos[counterPos]);
			counterPos++;
		}
		if(fscanf(readIt, "%s", NilaiNow.NRP) == EOF) break;
        fgetc(readIt);                              // Newline handling
		fscanf(readIt, "%s", NilaiNow.Kode);
		fgetc(readIt);
		fgets(NilaiNow.Matkul, 50, readIt);
		fscanf(readIt, "%s", &NilaiNow.Kelas);
		fgetc(readIt);
		fscanf(readIt, "%lf", &NilaiNow.Nilai);

		NilaiNow.Matkul[strlen(NilaiNow.Matkul)-1] = '\0';

		gotoxy(19, 4+(counter*5)+1); printf("NRP     : %s", NilaiNow.NRP);
		gotoxy(19, 4+(counter*5)+2); printf("KODE    : %s", NilaiNow.Kode);
		gotoxy(19, 4+(counter*5)+3); printf("KELAS   : %s %s", NilaiNow.Matkul, NilaiNow.Kelas);
		gotoxy(19, 4+(counter*5)+4); printf("NILAI   : %.4lf", NilaiNow.Nilai); 

		counter++;

		gotoxy(16, 4+16);
		if(counterPos != 1) printf(" < PREV ");
		else printf("%c%c%c%c%c%c%c%c", 196, 196, 196, 196 , 196, 196, 196, 196);
		gotoxy(WIDTH-24, 4+16); printf(" NEXT > ");

		if(counter == 3){
			gotoxy(WIDTH-18, 4+16);

			backAllNilai:
			switch(getch()){
				case 224:
					switch(getch()){
						case 77: 
							cleanTheBox();
							counter = 0;
							break;
						case 75:
							prevAllNilai:
							cleanTheBox();
							counter = 0;
							counterPos -= 2;
							if(counterPos < 0) counterPos = 0;
							fsetpos(readIt, &pos[counterPos]);
							break;
					}
					break;
				case 32:
					mainMenu();
					break;
				default:
					goto backAllNilai;
			}
		}
	}

	gotoxy(19, 4+(counter*5)+1); printf("TEKAN [SPASI] UNTUK KEMBALI KE MENU UTAMA");
	gotoxy(19, 4+(counter*5)+2); printf("TEKAN LAINNYA UNTUK KELUAR");

	gotoxy(19, 4+(counter*5)+4);
	switch(getch()){
		case 32: mainMenu(); break;
		case 224:
			if(getch() == 75) goto prevAllNilai;
	}

	fclose(readIt);
}

void mainMenu(){
	setupWindow();

	system("COLOR 0B");
	REP(i, HEIGHT){
		REP(j, WIDTH){
			gotoxy(j, i); printf("%c", 177);
		}
	}

	FORC(i, 3, HEIGHT-4){
		FORC(j, 15, WIDTH-15){
			gotoxy(j, i); printf("%c", 32);

			if(i == 3) 				{ gotoxy(j, i); printf("%c", 196);}
			if(j == 15)				{ gotoxy(j, i); printf("%c", 179);}
			if(i == (HEIGHT-4)-1) 	{ gotoxy(j, i); printf("%c", 196);}
			if(j == (WIDTH-15)-1)	{ gotoxy(j, i); printf("%c", 179);}

			if((i == 3) && (j == 15))						{ gotoxy(j, i); printf("%c", 218);}
			if((i == 3) && (j == (WIDTH-15)-1))				{ gotoxy(j, i); printf("%c", 191);}
			if((i == (HEIGHT-4)-1) && (j == 15))			{ gotoxy(j, i); printf("%c", 192);}
			if((i == (HEIGHT-4)-1) && (j == (WIDTH-15)-1))	{ gotoxy(j, i); printf("%c", 217);}
		}

	}

	gotoxy(19, 4+1); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 219, 219, 219, 219, 219, 219, 219, 187, 219, 219, 187, 219, 219, 219, 187,  32,  32,  32, 219, 219, 219, 187,  32,  32,  32, 219, 219, 219, 219, 219, 219, 219, 219, 187,  32, 219, 219, 219, 219, 219, 219, 187);
	gotoxy(19, 4+2); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 219, 219, 201, 205, 205, 205, 205, 188, 219, 219, 186, 219, 219, 219, 219, 187,  32, 219, 219, 219, 219, 186,  32,  32,  32, 200, 205, 205, 219, 219, 201, 205, 205, 188, 219, 219, 201, 205, 205, 205, 205, 188);
	gotoxy(19, 4+3); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 219, 219, 219, 219, 219, 219, 219, 187, 219, 219, 186, 219, 219, 201, 219, 219, 219, 219, 201, 219, 219, 186, 219, 219, 219, 219, 219, 187, 219, 219, 186,  32,  32,  32, 219, 219, 186,  32,  32,  32,  32,  32);
	gotoxy(19, 4+4); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 200, 205, 205, 205, 205, 219, 219, 186, 219, 219, 186, 219, 219, 186, 200, 219, 219, 201, 188, 219, 219, 186, 200, 205, 205, 205, 205, 188, 219, 219, 186,  32,  32,  32, 219, 219, 186,  32,  32,  32,  32,  32);
	gotoxy(19, 4+5); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 219, 219, 219, 219, 219, 219, 219, 186, 219, 219, 186, 219, 219, 186,  32, 200, 205, 188,  32, 219, 219, 186,  32,  32,  32,  32,  32,  32, 219, 219, 186,  32,  32,  32, 200, 219, 219, 219, 219, 219, 219, 187);
	gotoxy(19, 4+6); printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 200, 205, 205, 205, 205, 205, 205, 188, 200, 205, 188, 200, 205, 188,  32,  32,  32,  32,  32, 200, 205, 188,  32,  32,  32,  32,  32,  32, 200, 205, 188,  32,  32,  32,  32, 200, 205, 205, 205, 205, 205, 188);

	gotoxy(19, 4+8); printf(" SISTEM INFORMASI MAHASISWA   -   TC ITS\n");

	gotoxy(19, 4+11); printf("1. SI MAHASISWA");
	gotoxy(19, 4+12); printf("2. SI MATA KULIAH");
	gotoxy(19, 4+13); printf("3. SI NILAI");

	gotoxy(19, 4+15); printf("> ");

	switch(getch()){
		case '1':
			siMahasiswa();
			break;
		case '2':
			siMatkul();
			break;
		case '3':
			siNilai();
			break;

	}
}
