#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

// TO DO
// ( ) Add Interface!
// ( ) Pembobotan IPK berdasarkan SKS

// STRUKTUR DATA DASAR

typedef struct Mahasiswa{
	char    NRP[14];
	char    Nama[28];
	char    Doswal[28];
	double  IPK;
	int     SisaSKS;
	int		JumlahMatkul;
} Mahasiswa;

typedef struct MataKuliah {
	char    Kode[20];
	char    Nama[30];
	char	Dosen[28];
	char    Kelas[2];
	char	Ruang[6];
	int     SKS;
} Matkul;

typedef struct Nilai {
	char 	NRP[14];
	char	Matkul[30];
	char 	Kode[20];
	char	Kelas[2];
	double 	Nilai;
} Nilai;

// HANDLING MAHASISWA

void tambahMahasiswa(){
	Mahasiswa Maba;

	FILE * writeIt;
	writeIt = fopen("DB_MAHASISWA.dat", "a");

	fprintf(writeIt, "\n"); // Line Break

	printf("NRP : ");
	scanf("%s", Maba.NRP);

	fflush(stdin);

	printf("Nama : ");
	gets(Maba.Nama);

	fflush(stdin);

	printf("Doswal : ");
	gets(Maba.Doswal);

	Maba.IPK = 0.0;
	Maba.SisaSKS = 144;
	Maba.JumlahMatkul = 0;

	fprintf(writeIt, "%s\n", Maba.NRP);
	fprintf(writeIt, "%s\n", Maba.Nama);
	fprintf(writeIt, "%s\n", Maba.Doswal);
	fprintf(writeIt, "%lf\n", Maba.IPK);
	fprintf(writeIt, "%d\n", Maba.SisaSKS);
	fprintf(writeIt, "%d\n", Maba.JumlahMatkul);

	fclose(writeIt);
}

void lihatMahasiswa(char NRP[]){
    Mahasiswa MabaNow;
    int MabaDitemukan = 0;

    FILE * readIt;
    readIt = fopen("DB_MAHASISWA.dat", "r");

    while(fscanf(readIt, "%s", MabaNow.NRP) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(NRP, MabaNow.NRP) == 0){
            MabaDitemukan = 1;

            printf("%s\n", MabaNow.NRP);
            printf("%s", MabaNow.Nama);
            printf("%s", MabaNow.Doswal);
            printf("%lf\n", MabaNow.IPK);
            printf("%d\n", MabaNow.SisaSKS);
            printf("%d\n", MabaNow.JumlahMatkul);
		}
    }

    if(!MabaDitemukan) printf("Mahasiswa tidak ditemukan.\n");

    fclose(readIt);
}

void lihatSemuaMahasiswa(){
	Mahasiswa MabaNow;

	FILE * readIt;
	readIt = fopen("DB_MAHASISWA.dat", "r");

	while(fscanf(readIt, "%s", MabaNow.NRP) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		printf("%s\n", MabaNow.NRP);
		printf("%s", MabaNow.Nama);
		printf("%s", MabaNow.Doswal);
		printf("%lf\n", MabaNow.IPK);
		printf("%d\n", MabaNow.SisaSKS);
        printf("%d\n", MabaNow.JumlahMatkul);
	}

	fclose(readIt);
}

void hapusMahasiswa(char NRP[]){
	Mahasiswa MabaNow;
    int MabaDitemukan = 0;

    FILE * readIt;
    FILE * tempCopy;
    readIt   = fopen("DB_MAHASISWA.dat", "r");
    tempCopy = fopen("DB_MAHASISWA.COPY.dat", "w");

    while(fscanf(readIt, "%s", MabaNow.NRP) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, readIt);
		fgets(MabaNow.Doswal, 50, readIt);
		fscanf(readIt, "%lf", &MabaNow.IPK);
		fscanf(readIt, "%d", &MabaNow.SisaSKS);
		fscanf(readIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(NRP, MabaNow.NRP) != 0){
            MabaDitemukan = 1;

            fprintf(tempCopy, "\n");
            fprintf(tempCopy, "%s\n", MabaNow.NRP);
			fprintf(tempCopy, "%s", MabaNow.Nama);
			fprintf(tempCopy, "%s", MabaNow.Doswal);
			fprintf(tempCopy, "%lf\n", MabaNow.IPK);
			fprintf(tempCopy, "%d\n", MabaNow.SisaSKS);
			fprintf(tempCopy, "%d\n", MabaNow.JumlahMatkul);
		}
    }

    if(!MabaDitemukan) printf("Data mahasiswa tidak ditemukan.\n");

    fclose(readIt);
    fclose(tempCopy);

    readIt = fopen("DB_MAHASISWA.dat", "w");
    tempCopy = fopen("DB_MAHASISWA.COPY.dat", "r");

	while(fscanf(tempCopy, "%s", MabaNow.NRP) != EOF){
        fgetc(tempCopy);                              // Newline handling
		fgets(MabaNow.Nama, 50, tempCopy);
		fgets(MabaNow.Doswal, 50, tempCopy);
		fscanf(tempCopy, "%lf", &MabaNow.IPK);
		fscanf(tempCopy, "%d", &MabaNow.SisaSKS);
		fscanf(tempCopy, "%d", &MabaNow.JumlahMatkul);

        fprintf(readIt, "\n");
        fprintf(readIt, "%s\n", MabaNow.NRP);
		fprintf(readIt, "%s", MabaNow.Nama);
		fprintf(readIt, "%s", MabaNow.Doswal);
		fprintf(readIt, "%lf\n", MabaNow.IPK);
		fprintf(readIt, "%d\n", MabaNow.SisaSKS);
		fprintf(readIt, "%d\n", MabaNow.JumlahMatkul);
    }

    fclose(readIt);
    fclose(tempCopy);
}

// HANDLING MATKUL

void tambahMatkul(){
	Matkul MK;

	FILE * writeIt;
	writeIt = fopen("DB_MATKUL.dat", "a");

	fprintf(writeIt, "\n"); // Line Break

	printf("Kode : ");
	scanf("%s", MK.Kode);

	fflush(stdin);

	printf("Nama Matkul : ");
	gets(MK.Nama);

	fflush(stdin);

	printf("Dosen Pengajar : ");
	gets(MK.Dosen);

	fflush(stdin);

	printf("Kelas : ");
	scanf("%s", MK.Kelas);

	fflush(stdin);

	printf("Ruang Kelas : ");
	scanf("%s", MK.Ruang);

	fflush(stdin);

	printf("SKS : ");
	scanf("%d", &MK.SKS);

	fprintf(writeIt, "%s\n", MK.Kode);
	fprintf(writeIt, "%s\n", MK.Nama);
	fprintf(writeIt, "%s\n", MK.Dosen);
	fprintf(writeIt, "%s\n", MK.Kelas);
	fprintf(writeIt, "%s\n", MK.Ruang);
	fprintf(writeIt, "%d\n", MK.SKS);

	fclose(writeIt);
}

void lihatMatkul(char Kode[], char Kelas[]){
	Matkul MK;
    int MatkulDitemukan = 0;

    FILE * readIt;
    readIt = fopen("DB_MATKUL.dat", "r");

    while(fscanf(readIt, "%s", MK.Kode) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fgetc(readIt);
		fscanf(readIt, "%s", MK.Ruang);
		fgetc(readIt);
		fscanf(readIt, "%d", &MK.SKS);

		if((strcmp(Kode, MK.Kode) == 0) &&(strcmp(Kelas, MK.Kelas) == 0)){
            MatkulDitemukan = 1;

            printf("%s\n", MK.Kode);
            printf("%s", MK.Nama);
            printf("%s", MK.Dosen);
            printf("%s\n", MK.Kelas);
            printf("%s\n", MK.Ruang);
            printf("%d\n", MK.SKS);
		}
    }

    if(!MatkulDitemukan) printf("Kelas tidak ditemukan.\n");

    fclose(readIt);
}

void hapusMatkul(char * Kode){
	Matkul MK;
    int MatkulDitemukan = 0;

    FILE * readIt;
    FILE * tempCopy;
    readIt   = fopen("DB_MATKUL.dat", "r");
    tempCopy = fopen("DB_MATKUL.COPY.dat", "w");

    while(fscanf(readIt, "%s", MK.Kode) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fscanf(readIt, "%s", MK.Ruang);
		fscanf(readIt, "%d", &MK.SKS);

		if(strcmp(Kode, MK.Kode) != 0){
            MatkulDitemukan = 1;

            fprintf(tempCopy, "\n");
            fprintf(tempCopy, "%s\n", MK.Kode);
			fprintf(tempCopy, "%s", MK.Nama);
			fprintf(tempCopy, "%s", MK.Dosen);
			fprintf(tempCopy, "%s\n", MK.Kelas);
			fprintf(tempCopy, "%s\n", MK.Ruang);
			fprintf(tempCopy, "%d\n", MK.SKS);
		}
    }

    if(!MatkulDitemukan) printf("Matkul tidak ditemukan.\n");

    fclose(readIt);
    fclose(tempCopy);

    tempCopy = fopen("DB_MATKUL.dat", "w");
    readIt = fopen("DB_MATKUL.COPY.dat", "r");

    while(fscanf(readIt, "%s", MK.Kode) != EOF){
        fgetc(readIt);                              // Newline handling
		fgets(MK.Nama, 50, readIt);
		fgets(MK.Dosen, 50, readIt);
		fscanf(readIt, "%s", MK.Kelas);
		fscanf(readIt, "%s", MK.Ruang);
		fscanf(readIt, "%d", &MK.SKS);

        fprintf(tempCopy, "\n");
        fprintf(tempCopy, "%s\n", MK.Kode);
		fprintf(tempCopy, "%s", MK.Nama);
		fprintf(tempCopy, "%s", MK.Dosen);
		fprintf(tempCopy, "%s\n", MK.Kelas);
		fprintf(tempCopy, "%s\n", MK.Ruang);
		fprintf(tempCopy, "%d\n", MK.SKS);
    }

    fclose(readIt);
    fclose(tempCopy);
}

// HANDLING NILAI

void tambahNilai(){
	Nilai NilaiBaru;

	printf("Masukkan NRP mahasiswa : ");
	scanf("%s", NilaiBaru.NRP);

	fflush(stdin);

	printf("Masukkan nama matkul : ");
	gets(NilaiBaru.Matkul);

	fflush(stdin);

	do {
		printf("Masukkan nilai yang diperoleh (Skala 4) : ");
		scanf("%lf", &NilaiBaru.Nilai);
	} while((NilaiBaru.Nilai < 0) || (NilaiBaru.Nilai > 4));

	fflush(stdin);

	Nilai NilaiMaba;

	FILE * changeIt;
	FILE * changed;

	changeIt = fopen("DB_NILAI.dat", "r+");
	changed  = fopen("DB_NILAI.COPY.dat", "w");

	while(fscanf(changeIt, "%s", NilaiMaba.NRP) != EOF){
		fgetc(changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kode);
		fgetc(changeIt);
		fgets(NilaiMaba.Matkul, 50, changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kelas);
		fgetc(changeIt);
		fscanf(changeIt, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiBaru.NRP, NilaiMaba.NRP) == 0) && (strcmp(NilaiBaru.Matkul, NilaiMaba.Matkul) == 0)){
			NilaiMaba.Nilai = NilaiBaru.Nilai;
		}

	    fprintf(changed, "\n");
	    fprintf(changed, "%s\n", 	NilaiMaba.NRP);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kode);
	    fprintf(changed, "%s\n", 	NilaiMaba.Matkul);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kelas);
	    fprintf(changed, "%lf\n",   NilaiMaba.Nilai);
	}

	fclose(changeIt);
	fclose(changed);

	changed = fopen("DB_NILAI.dat", "w");
	changeIt  = fopen("DB_NILAI.COPY.dat", "r");

	while(fscanf(changeIt, "%s", NilaiMaba.NRP) != EOF){
		fgetc(changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kode);
		fgetc(changeIt);
		fgets(NilaiMaba.Matkul, 50, changeIt);
		fscanf(changeIt, "%s", NilaiMaba.Kelas);
		fgetc(changeIt);
		fscanf(changeIt, "%lf", &NilaiMaba.Nilai);

	    fprintf(changed, "\n");
	    fprintf(changed, "%s\n", 	NilaiMaba.NRP);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kode);
	    fprintf(changed, "%s",	 	NilaiMaba.Matkul);
	    fprintf(changed, "%s\n", 	NilaiMaba.Kelas);
	    fprintf(changed, "%lf\n",   NilaiMaba.Nilai);
	}

	fclose(changeIt);
	fclose(changed);

	Mahasiswa MabaNow;

	changeIt = fopen("DB_MAHASISWA.dat", "r");
	changed  = fopen("DB_MAHASISWA.COPY.dat", "w");

	while(fscanf(changeIt, "%s", MabaNow.NRP) != EOF){
        fgetc(changeIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, changeIt);
		fgets(MabaNow.Doswal, 50, changeIt);
		fscanf(changeIt, "%lf", &MabaNow.IPK);
		fscanf(changeIt, "%d", &MabaNow.SisaSKS);
		fscanf(changeIt, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(MabaNow.NRP, NilaiBaru.NRP) == 0){
			MabaNow.JumlahMatkul += 1;
			MabaNow.IPK = (MabaNow.IPK * (MabaNow.JumlahMatkul-1)) + NilaiBaru.Nilai;
			MabaNow.IPK = MabaNow.IPK/MabaNow.JumlahMatkul;
		}

		fprintf(changed, "\n");
		fprintf(changed, "%s\n", MabaNow.NRP);
		fprintf(changed, "%s", MabaNow.Nama);
		fprintf(changed, "%s", MabaNow.Doswal);
		fprintf(changed, "%lf\n", MabaNow.IPK);
		fprintf(changed, "%d\n", MabaNow.SisaSKS);
		fprintf(changed, "%d\n", MabaNow.JumlahMatkul);
	}	

	fclose(changed);
	fclose(changeIt);

	changed = fopen("DB_MAHASISWA.dat", "w");
	changeIt  = fopen("DB_MAHASISWA.COPY.dat", "r");

	while(fscanf(changeIt, "%s", MabaNow.NRP) != EOF){
        fgetc(changeIt);                              // Newline handling
		fgets(MabaNow.Nama, 50, changeIt);
		fgets(MabaNow.Doswal, 50, changeIt);
		fscanf(changeIt, "%lf", &MabaNow.IPK);
		fscanf(changeIt, "%d", &MabaNow.SisaSKS);
		fscanf(changeIt, "%d", &MabaNow.JumlahMatkul);

		fprintf(changed, "\n");
		fprintf(changed, "%s\n", MabaNow.NRP);
		fprintf(changed, "%s", MabaNow.Nama);
		fprintf(changed, "%s", MabaNow.Doswal);
		fprintf(changed, "%lf\n", MabaNow.IPK);
		fprintf(changed, "%d\n", MabaNow.SisaSKS);
		fprintf(changed, "%d\n", MabaNow.JumlahMatkul);
	}	

	fclose(changed);
	fclose(changeIt);
}

// HANDLING FRS

void ambilMatkul(){
	Nilai FRS;
	Matkul MK;

	printf("Masukkan NRP anda : ");
	scanf("%s", FRS.NRP);

	fflush(stdin);

	printf("Masukkan matkul yang akan di ambil : ");
	gets(FRS.Matkul);

	fflush(stdin);

	printf("Masukkan kelas yang anda inginkan : ");
	gets(FRS.Kelas);

	fflush(stdin);

	FILE * searchIt;
	FILE * modifySKS;
	FILE * modifiedSKS;

	Mahasiswa MabaNow;
	int SKSTerambil = 0;

	searchIt  = fopen("DB_MATKUL.dat", "r");
	modifySKS = fopen("DB_MAHASISWA.dat", "r");
	modifiedSKS = fopen("DB_MAHASISWA.COPY.dat", "w");

	while(fscanf(searchIt, "%s", MK.Kode) != EOF){
        fgetc(searchIt);                              // Newline handling
		fgets(MK.Nama, 50, searchIt);
		fgets(MK.Dosen, 50, searchIt);
		fscanf(searchIt, "%s", MK.Kelas);
		fgetc(searchIt);
		fscanf(searchIt, "%s", MK.Ruang);
		fgetc(searchIt);
		fscanf(searchIt, "%d", &MK.SKS);

		MK.Nama[strlen(MK.Nama) - 1] = '\0';		// Penghapusan \n

		if(strcmp(FRS.Matkul, MK.Nama) == 0){
			strcpy(FRS.Kode, MK.Kode);
			SKSTerambil = MK.SKS;
		}
    }

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		if(strcmp(MabaNow.NRP, FRS.NRP) == 0){
			MabaNow.SisaSKS -= SKSTerambil;
		}

		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(searchIt);
    fclose(modifySKS);
    fclose(modifiedSKS);

    modifySKS   = fopen("DB_MAHASISWA.COPY.dat", "r");
    modifiedSKS = fopen("DB_MAHASISWA.dat", "w");

    while(fscanf(modifySKS, "%s", MabaNow.NRP) != EOF){
        fgetc(modifySKS);                              // Newline handling
		fgets(MabaNow.Nama, 50, modifySKS);
		fgets(MabaNow.Doswal, 50, modifySKS);
		fscanf(modifySKS, "%lf", &MabaNow.IPK);
		fscanf(modifySKS, "%d", &MabaNow.SisaSKS);
		fscanf(modifySKS, "%d", &MabaNow.JumlahMatkul);

		fprintf(modifiedSKS, "\n");
		fprintf(modifiedSKS, "%s\n", MabaNow.NRP);
		fprintf(modifiedSKS, "%s", MabaNow.Nama);
		fprintf(modifiedSKS, "%s", MabaNow.Doswal);
		fprintf(modifiedSKS, "%lf\n", MabaNow.IPK);
		fprintf(modifiedSKS, "%d\n", MabaNow.SisaSKS);
		fprintf(modifiedSKS, "%d\n", MabaNow.JumlahMatkul);
	}

    fclose(modifySKS);
    fclose(modifiedSKS);

    FILE * writeIt;
    writeIt = fopen("DB_NILAI.dat", "a");

    fprintf(writeIt, "\n");
    fprintf(writeIt, "%s\n", 	FRS.NRP);
    fprintf(writeIt, "%s\n", 	FRS.Kode);
    fprintf(writeIt, "%s\n", 	FRS.Matkul);
    fprintf(writeIt, "%s\n", 	FRS.Kelas);
    fprintf(writeIt, "%lf\n",   -1.0);

    fclose(writeIt);
}


// AAA

void matkulDiambilOleh(char * NRP){
	Nilai NilaiMaba;
	
	FILE * cariMatkul;
	cariMatkul = fopen("DB_NILAI.dat", "r");

	while(fscanf(cariMatkul, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariMatkul);
		fscanf(cariMatkul, "%s", NilaiMaba.Kode);
		fgetc(cariMatkul);
		fgets(NilaiMaba.Matkul, 50, cariMatkul);
		fscanf(cariMatkul, "%s", NilaiMaba.Kelas);
		fgetc(cariMatkul);
		fscanf(cariMatkul, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if(strcmp(NilaiMaba.NRP, NRP) == 0){
			printf("%s %s\n", NilaiMaba.Matkul, NilaiMaba.Kelas);
		}
	}
}

// BBB

void mahasiswaMengambilMatkul(char * Matkul, char * Kelas){
	Nilai NilaiMaba;
	
	FILE * cariMaba;
	cariMaba = fopen("DB_NILAI.dat", "r");

	while(fscanf(cariMaba, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariMaba);
		fscanf(cariMaba, "%s", NilaiMaba.Kode);
		fgetc(cariMaba);
		fgets(NilaiMaba.Matkul, 50, cariMaba);
		fscanf(cariMaba, "%s", NilaiMaba.Kelas);
		fgetc(cariMaba);
		fscanf(cariMaba, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiMaba.Matkul, Matkul) == 0) && (strcmp(NilaiMaba.Kelas, Kelas) == 0)){
			printf("%s\n", NilaiMaba.NRP);
		}
	}
}

// CCC

void nilaiDari(char * NRP, char * Kode){
	Nilai NilaiMaba;
	
	FILE * cariNilai;
	cariNilai = fopen("DB_NILAI.dat", "r");

	while(fscanf(cariNilai, "%s", NilaiMaba.NRP) != EOF){
		fgetc(cariNilai);
		fscanf(cariNilai, "%s", NilaiMaba.Kode);
		fgetc(cariNilai);
		fgets(NilaiMaba.Matkul, 50, cariNilai);
		fscanf(cariNilai, "%s", NilaiMaba.Kelas);
		fgetc(cariNilai);
		fscanf(cariNilai, "%lf", &NilaiMaba.Nilai);

		NilaiMaba.Matkul[strlen(NilaiMaba.Matkul)-1] = '\0';

		if((strcmp(NilaiMaba.NRP, NRP) == 0) && (strcmp(NilaiMaba.Kode, Kode) == 0)){
			printf("Nilai : %lf\n", NilaiMaba.Nilai);
		}
	}
}

int main(){
    tambahMahasiswa();
    tambahMatkul();
    ambilMatkul();

//	lihatSemuaMahasiswa();
}